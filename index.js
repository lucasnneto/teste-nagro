var link = 'https://jsonplaceholder.typicode.com/users/';
//var link = "http://localhost:3000/users/";
var serial = new Vue();

var tab = Vue.component("tabela", {
    template: "#tabela",
    data() {
        return {
            
            Us: [],
            pesquisa: "",
            img: "https://static.vecteezy.com/system/resources/previews/000/512/576/non_2x/vector-profile-glyph-black-icon.jpg"
        }
    },
    props: {
        items: {
            type: Array,
        },
    },
    methods: {
        onClickRow(index) {
            app.mode = "usuario";
            var mv = this;
            var url = link + index;
            this.$http.get(url).then(function (response) {
                mv.Us = response.body;
                app.titulo = mv.Us.name;
                serial.$emit('send', mv.Us);
            }, function (erro) {
                console.log("ERRO");
            });
        },
    },
    computed: {
        filterNames: function () {
            var search = this.pesquisa.toLowerCase();
            return this.items.filter((item) => {
                return item.name.toLowerCase().match(search) || item.username.toLowerCase().match(search)
            });
        }
    },
})

var ed = Vue.component("editar", {
    template: "#editar",
    data() {
        return {
            envio:false,
            notif: { sit: 'no', valor: '', spin: false },
            id: "",
            usuario: {
                id: "",
                name: '',
                username: '',
                email: '',
                address: {
                    street: '',
                    suite: '',
                    city: '',
                    zipcode: '',
                    geo: {
                        lat: '',
                        lng: ''
                    }
                },
                phone: '',
                website: '',
                company: {
                    name: '',
                    catchPhrase: '',
                    bs: ''
                }
            }
        }
    },
    created() {
        this.newId()
    },

    methods: {
        newId() {
            this.id = "" + Math.floor(Math.random() * 100 + 10)
            this.usuario.id = this.id
        },
        Save() {
            this.envio=true
            if (this.Send()) {
                var vm = this
                var url = link
                this.notif.spin = true
                this.$http.post(url, this.usuario).then(function (envio) {
                    vm.notif.sit = 'sucesso'
                    vm.notif.valor = 'Salvo'
                    vm.notif.spin = false
                    vm.Clear()
                    vm.newId()
                }, function (erro) {
                    vm.notif.sit = 'danger'
                    vm.notif.valor = 'Salvar'
                    vm.notif.spin = false
                })
            }


        },
        Clear() {
            this.envio = false
            this.usuario = {
                id: this.id,
                name: '',
                username: '',
                email: '',
                address: {
                    street: '',
                    suite: '',
                    city: '',
                    zipcode: '',
                    geo: {
                        lat: '',
                        lng: ''
                    }
                },
                phone: '',
                website: '',
                company: {
                    name: '',
                    catchPhrase: '',
                    bs: ''
                }
            }
        },
        Create() {
            this.envio=false
            this.usuario = {
                id: this.id,
                name: "Diogo Silva",
                username: 'dsilvs',
                email: 'diogo-silva@email.com',
                address: {
                    street: 'Rua Céu Azul',
                    suite: 'Numero 130, apartamento 15',
                    city: 'Monte Belo',
                    zipcode: '917020-112',
                    geo: {
                        lat: '-17.829067',
                        lng: '-55.856355'
                    }
                },
                phone: '(99)9888-1234',
                website: 'dsilva.com',
                company: {
                    name: 'Robel-Corkery',
                    catchPhrase: 'Multi-tiered zero tolerance productivity',
                    bs: 'transition cutting-edge web services'
                }
            }
        },
        Send() {
            
            const nameisValid = this.nameisValid
            const usernameisValid = this.usernameisValid
            const emailisValid = this.emailisValid
            const streetisValid = this.streetisValid
            const suiteisValid = this.suiteisValid
            const cityisValid = this.cityisValid
            const zipcodeisValid = this.zipcodeisValid
            const latisValid = this.latisValid
            const lngisValid = this.lngisValid
            const phoneisValid = this.phoneisValid
            const websiteisValid = this.websiteisValid
            const cnameisValid = this.cnameisValid
            const cphraseisValid = this.cphraseisValid
            const cbsisValid = this.cbsisValid

            return (nameisValid && usernameisValid && emailisValid && streetisValid && suiteisValid
                && cityisValid && zipcodeisValid && latisValid && lngisValid && phoneisValid
                && websiteisValid && cnameisValid && cphraseisValid && cbsisValid)
        }

    },
   
    computed:{
        nameisValid(){
            if(this.envio) return this.usuario.name != ''
            else return true
        },
        usernameisValid(){
            if(this.envio) return this.usuario.username != ''
            else return true
        },
        emailisValid(){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(this.envio) return this.usuario.email != '' && re.test(this.usuario.email)
            else return true
        },
        streetisValid(){
            if(this.envio) return this.usuario.address.street != ''
            else return true
        },
        suiteisValid(){
            if(this.envio) return this.usuario.address.suite != ''
            else return true
        },
        cityisValid(){
            if(this.envio) return this.usuario.address.city != ''
            else return true
        },
        zipcodeisValid(){
            if(this.envio) return this.usuario.address.zipcode != ''
            else return true
        },
        latisValid(){
            if(this.envio) return this.usuario.address.geo.lat != ''
            else return true
        },
        lngisValid(){
            if(this.envio) return this.usuario.address.geo.lng != ''
            else return true
        },
        phoneisValid(){
            if(this.envio) return this.usuario.phone != ''
            else return true
        },
        websiteisValid(){
            if(this.envio) return this.usuario.website != ''
            else return true
        },
        cnameisValid(){
            if(this.envio) return this.usuario.company.name != ''
            else return true
        },
        cphraseisValid(){
            if(this.envio) return this.usuario.company.catchPhrase != ''
            else return true
        },
        cbsisValid(){
            if(this.envio) return this.usuario.company.bs != ''
            else return true
        },
        
    }
})

var us = Vue.component("usuario", {
    template: "#usuario",
    data() {
        return {
            envio:false,
            dados: {
                id: "",
                name: '',
                username: '',
                email: '',
                address: {
                    street: '',
                    suite: '',
                    city: '',
                    zipcode: '',
                    geo: {
                        lat: '',
                        lng: ''
                    }
                },
                phone: '',
                website: '',
                company: {
                    name: '',
                    catchPhrase: '',
                    bs: ''
                }
            },
            notif: { sit: 'no', valor: '', spin: false, spin2: false }
        }
    },
    created() {
        this.envio=false
        
        var vm = this;
        serial.$on('send', function (tex) {
            if (tex) {
                vm.dados = tex;
            }
        });
    },
    methods: {
        DeleteUser() {
            this.envio = false
            var vm = this
            var url = link + this.dados.id;
            this.notif.spin = true
            this.$http.delete(url).then(function (envio) {

                vm.notif.sit = 'sucesso'
                vm.notif.valor = 'Removido'
                vm.notif.spin = false
                app.Var.splice(vm.dados.id-1,1)
                app.mode='tabela'
            }, function (erro) {

                vm.notif.sit = 'danger'
                vm.notif.valor = 'Remover'
                vm.notif.spin = false
            });
        },
        UpdateUser() {
            this.envio=true
            if (this.Send()){
            var vm = this
            this.notif.spin2 = true
            var url = link + this.dados.id;
            this.$http.put(url, this.dados).then(function (envio) {
                vm.notif.sit = 'sucesso'
                vm.notif.valor = 'Atualizado'
                vm.notif.spin2 = false
                app.Var[this.dados.id-1]= this.dados
            }, function (erro) {
                vm.notif.sit = 'danger'
                vm.notif.valor = 'Salvar'
                vm.notif.spin2 = false
            })}
        },
        Send() {
            
            const nameisValid = this.nameisValid
            const usernameisValid = this.usernameisValid
            const emailisValid = this.emailisValid
            const streetisValid = this.streetisValid
            const suiteisValid = this.suiteisValid
            const cityisValid = this.cityisValid
            const zipcodeisValid = this.zipcodeisValid
            const latisValid = this.latisValid
            const lngisValid = this.lngisValid
            const phoneisValid = this.phoneisValid
            const websiteisValid = this.websiteisValid
            const cnameisValid = this.cnameisValid
            const cphraseisValid = this.cphraseisValid
            const cbsisValid = this.cbsisValid

            return (nameisValid && usernameisValid && emailisValid && streetisValid && suiteisValid
                && cityisValid && zipcodeisValid && latisValid && lngisValid && phoneisValid
                && websiteisValid && cnameisValid && cphraseisValid && cbsisValid)
        }
    },
    computed:{
        nameisValid(){
            if(this.envio) return this.dados.name != ''
            else return true
        },
        usernameisValid(){
            if(this.envio) return this.dados.username != ''
            else return true
        },
        emailisValid(){
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(this.envio) return this.dados.email != '' && re.test(this.dados.email)
            else return true
        },
        streetisValid(){
            if(this.envio) return this.dados.address.street != ''
            else return true
        },
        suiteisValid(){
            if(this.envio) return this.dados.address.suite != ''
            else return true
        },
        cityisValid(){
            if(this.envio) return this.dados.address.city != ''
            else return true
        },
        zipcodeisValid(){
            if(this.envio) return this.dados.address.zipcode != ''
            else return true
        },
        latisValid(){
            if(this.envio) return this.dados.address.geo.lat != ''
            else return true
        },
        lngisValid(){
            if(this.envio) return this.dados.address.geo.lng != ''
            else return true
        },
        phoneisValid(){
            if(this.envio) return this.dados.phone != ''
            else return true
        },
        websiteisValid(){
            if(this.envio) return this.dados.website != ''
            else return true
        },
        cnameisValid(){
            if(this.envio) return this.dados.company.name != ''
            else return true
        },
        cphraseisValid(){
            if(this.envio) return this.dados.company.catchPhrase != ''
            else return true
        },
        cbsisValid(){
            if(this.envio) return this.dados.company.bs != ''
            else return true
        },
        
    }
})

var app = new Vue({
    el: '#app',
    data: {
        Var: [],
        mode: "tabela",
    },
    created() {
        var mv = this;
        var url = link;
        this.$http.get(url).then(function (response) {
            mv.Var = response.body;
        }, function (erro) {
            console.log("ERRO");
        });
    },
})